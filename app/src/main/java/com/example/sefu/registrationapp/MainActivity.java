package com.example.sefu.registrationapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class MainActivity extends AppCompatActivity {
    String[] courses = {"Computer Science", "Arts", "Education"};
    ArrayAdapter<String> adapter;
    AutoCompleteTextView coursesAutoCompleteTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice, courses);

        coursesAutoCompleteTextView=(AutoCompleteTextView)findViewById(R.id.courses);
        //Set the number of characters the user must type before the drop down list is shown
        coursesAutoCompleteTextView.setThreshold(1);
        //Set the adapter
        coursesAutoCompleteTextView.setAdapter(adapter);
    }
}
